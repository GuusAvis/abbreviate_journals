import sys, os
import re
import csv

try:
    bibtexfilename = sys.argv[1]
except:
    raise ValueError("Error: specify the file to be processed!")
if bibtexfilename[-4:] != ".bib":
    raise ValueError("Filename must be a bibtex file, and should end in .bib.")

if len(sys.argv) > 2:
    abbreviations_file_name = sys.argv[2]
else:
    abbreviations_file_name = "combined_list_of_journal_abbreviations.csv"
if not os.path.isfile(abbreviations_file_name):
    raise FileNotFoundError(f"Cannot find file {abbreviations_file_name}.")
abbreviations = {}
with open(abbreviations_file_name, mode='r', encoding='utf-8') as file:
    csv_reader = csv.reader(file)
    for row in csv_reader:
        abbreviations[row[0].strip().lower()] = row[1]

# Read the contents of the bibtexdb text file and split it into lines
with open(bibtexfilename, 'r') as bibtexfile:
    bibtexdb = bibtexfile.readlines()

# Extract lines starting with "journal" to avoid replacements in other lines
journal_lines = [line for line in bibtexdb if line.strip().startswith("journal")]

for i, line in enumerate(journal_lines):
    journal = line.strip().split("{")[1].split("}")[0]
    journal_simplified = journal.strip().lower()
    abbreviation = abbreviations.get(journal_simplified, None)
    if abbreviation is not None and abbreviation != journal:
        repl = re.compile(re.escape(journal), re.IGNORECASE)
        line, _ = repl.subn(abbreviation, line)
        journal_lines[i] = line
        print(f"replaced {journal} with {abbreviation}")

# Update the original bibtexdb with modified journal_lines
for i, line in enumerate(bibtexdb):
    if line.strip().startswith("journal"):
        bibtexdb[i] = journal_lines.pop(0)

# Write the modified bibtexdb back to the file
new_file_name = bibtexfilename[:-4]  # remove ".bib" suffix
new_file_name = new_file_name + "_abbreviated.bib"
with open(new_file_name, 'w') as bibtexfile:
    bibtexfile.writelines(bibtexdb)
print(f"Abbreviated bib file saved as {new_file_name}.")
