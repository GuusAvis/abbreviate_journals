import sys
import pandas as pd

JOURNAL_LISTS = ["journal_abbreviations_acs.csv", "journal_abbreviations_aea.csv", "journal_abbreviations_ams.csv",
                 "journal_abbreviations_annee-philologique.csv", "journal_abbreviations_dainst.csv",
                 "journal_abbreviations_general.csv", "journal_abbreviations_geology_physics.csv", "journal_abbreviations_ieee.csv",
                 "journal_abbreviations_lifescience.csv", "journal_abbreviations_mathematics.csv", "journal_abbreviations_mechanical.csv",
                 "journal_abbreviations_medicus.csv", "journal_abbreviations_meteorology.csv", "journal_abbreviations_sociology.csv"
                 ]
DEFAULT_JOURNAL_FOLDER = "abbrv.jabref.org/journals"

def main(journal_folder):
    # Read and merge CSV files
    dfs = []
    for journal_list in JOURNAL_LISTS:
        file = f"{journal_folder}/{journal_list}"
        df = pd.read_csv(file, header=None)
        dfs.append(df)
        print(f"{file}: {len(df)}")
    merged_df = pd.concat(dfs, ignore_index=True)

    # Drop duplicates based on the first column value and keep the last one obtained
    merged_df.drop_duplicates(subset=[0], keep='last', inplace=True)

    # Sort alphabetically
    sorted_df = merged_df.sort_values(by=[0])

    # Save the result to the specified CSV file and ensure values are quoted
    sorted_df.to_csv("combined_list_of_journal_abbreviations.csv", index=False, header=False, quoting=1)

    print(f"Combined journal lists from {journal_folder} into combined_list_of_journal_abbreviations.csv, Combined key count: {len(merged_df)}")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        journal_folder = sys.argv[1]
    else:
        journal_folder = DEFAULT_JOURNAL_FOLDER

    main(journal_folder)