This is a python script for replacing journal titles in a bibtex file by their standard journal abbreviations.
It makes use of the lists provided by the [abbrv.jabref.org](https://abbrv.jabref.org/) project (these are included in this repo as a git submodule).
For every field in the bibtex file that starts with `journal`, the value is compared against the provided lists and replaced by an abbreviated journal name if possible.

Note that jabref can also be used to automatically add journal abbreviations in accordance with the lists there provided.
However, JabRef is a full-blown reference manager, and may be considered a too-large and too-complicated tool for the simple task of replacing journal titles by abbreviations.
For instance, it may also make changes to your bibtex file that you didn't need or want, such as reformatting or sorting.
Additionally, JabRef automatically uses all the abbreviation lists included in abbrv.jabref.org.
This tool, on the other hand, allows you to easily decide which of those lists you want to use.
Excluding lists can be useful when there are abbreviation rules in some of the lists that you don't like.
For instance, the code here automatically excludes the astronomy list, as it leads to abbreviating the journal "Nature" as "Nat" and "Science" as "Sci", both of which are non-standard abbreviations.
It also excludes the "entrez" list, as it contains abbreviations that are already included in other lists, but then without periods after abbreviated words (e.g., Phys Rev Lett instead of Phys. Rev. Lett.).
If the abbreviations you get out don't match your expectations, you can play around with which lists you include (see instructions below).
Adding custom lists is also an easy matter.

This code is based on a [script](https://gist.github.com/arahatashun/2d27d5e2694c3f1f25aa5cd624defe5f) by the github user arahatashun.
As opposed to that script, this one is compatible with the latest journal lists from abbrv.jabref.org (these have moved location in the meantime, and switched to a csv format) and is easily kept up to date by including them as a submodule.

## Usage
1. Clone this repository.
2. Initialize the abbrv.jabref.org submodule (you can do this in the terminal by entering `git submodule init` while in the root folder of the cloned repository).
3. Update the abbrv.jabref.org submodule (you can do this in the terminal by entering `git submodule update` while in the root folder of the cloned repository).
4. Run `python3 combine_journal_abbreviations.py` from the root folder to combine the journal lists in the abbrv.jebref.org submodule into the `combined_list_of_journal_abbreviations.csv` file. If you want to run from another folder, you can add the folder holding the to-be-combined lists as a parameter when calling the python script. Additionally, which lists from the folder should be combined can be chosen by changing the `JOURNAL_LISTS` parameter defined directly after the import statements in the script.
5. Run `python3 abbreviate.py test_bib.bib` from the root folder to create `test_bib_abbreviated.bib` (where `test_bib.bib` should be replaced by the bibtex file where you want to abbreviate journal titles). If you want to use an abbreviation list other than the one created by `combine_journal_abbreviations`, or you want to run from some other folder, you can add a second argument to specify the list that should be used (this should be a csv file).

Note: to stay up to date with changes in abbrv.jabref.org, it is recommended to regularly execute steps 3 and 4 again before using `abbreviate.py`.
